#include <WiFiEsp.h>
#include <OneWire.h>                
#include <DallasTemperature.h>
#define trigPin 6
#define echoPin 7
OneWire ourWire(13);                //Se establece el pin 2  como bus OneWire
DallasTemperature sensors(&ourWire); 
#include "SoftwareSerial.h"
SoftwareSerial Serial1(10,11);
char ssid[] = "TELE";
char pass[] = "54321728";
int status = WL_IDLE_STATUS;
WiFiEspClient client;
const int sensorPin = 2;
const int measureInterval = 2500;
volatile int pulseConter;
const float factorK = 7.5;
void ISRCountPulse()
{
   pulseConter++;
}
 
float GetFrequency()
{
   pulseConter = 0; 
   delay(measureInterval);
   return (float)pulseConter * 1000 / measureInterval;
}
void setup() {
  attachInterrupt(digitalPinToInterrupt(sensorPin), ISRCountPulse, RISING);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT); 
  sensors.begin();
  Serial1.begin(9600);
  Serial.begin(9600);
  WiFi.init(&Serial1);
  if(WiFi.status() == WL_NO_SHIELD){
      Serial.println("Modulo no presente");
      while(true);
    }
  while(status != WL_CONNECTED){
    Serial.print("Intentando conecta a la red wifi: ");
    Serial.println(ssid);
    status= WiFi.begin(ssid, pass);
  }
}
String c;
int pos;
long duracion, distancia;
float temperatura;
void loop() {
  sensors.requestTemperatures();   //Se envía el comando para leer la temperatura
  temperatura= sensors.getTempCByIndex(0); //Se obtiene la temperatura en ºC //TEMPERATURA
  // obtener frecuencia en Hz
  float frequency = GetFrequency();
  
  // calcular caudal L/min
  float flujo = frequency / factorK;
  digitalWrite(trigPin, LOW);        // Nos aseguramos de que el trigger está desactivado
  delayMicroseconds(2);              // Para asegurarnos de que el trigger esta LOW
  digitalWrite(trigPin, HIGH);       // Activamos el pulso de salida
  delayMicroseconds(10);             // Esperamos 10µs. El pulso sigue active este tiempo
  digitalWrite(trigPin, LOW);        // Cortamos el pulso y a esperar el echo
  duracion = pulseIn(echoPin, HIGH) ;
  distancia = duracion/2/29.1;
  Serial.println(String(temperatura));
  Serial.println();
  if(client.connect("us-central1-pruebaangular-6383e.cloudfunctions.net",80)){
    Serial.println("Conectado al servidor");
    client.println("GET /ingresar_datos_con_obstruido?valor_energia="+String(flujo)+"&valor_temperatura="+String(temperatura)+"&valor_nivel="+String(distancia)+"&valor_obstruido=no HTTP/1.1\r\nHost: us-central1-pruebaangular-6383e.cloudfunctions.net");
    client.println();
    if(client.connected()){
      while(c = client.readStringUntil('\r')) {
        if(c != ""){
          pos=c.indexOf("cerrar_compuerta");
          if (pos>=0) {
            digitalWrite(7,HIGH);
            Serial.println("Apagué");
          }
          pos=c.indexOf("abrir_compuerta");
          if (pos>=0) {
            digitalWrite(7,LOW);
            Serial.println("Prendí");
          }
        }else{
         break;
        }
      }
    }
    client.stop();
  }else{
  Serial.print("Conexion Fallida");
  }
  delay(3000);
}
